//BudgetController

var budgetController = (function (){
    var Expense = function (id , descr , value ){
        this.id =id;
        this.descr =descr;
        this.value = value; 
		this.percentage = -1;
    };
	
	Expense.prototype.calcPercentage = function(totalincome){
		
		if(totalincome > 0){
			this.percentage = Math.round((this.value/totalincome)*100);
		}
		else {
			this.percentage = -1;
		}
		
		
	};
		Expense.prototype.getExpensePerc = function(){
			return this.percentage ;
		}

    var Income = function (id , descr , value ){
        this.id = id;
        this.descr = descr;
        this.value = value;  
    };
     
   var calculateTotal = function (type) {
             var sum =0
             dataItems.allItems[type].forEach(function(curr){
                    sum += curr.value;   
             });
             dataItems.totals[type] = sum;
   };   
    
    var  dataItems = {
         allItems : {
         exp : [], 
         inc : []                   
                   },
              
          totals : {
            exp : 0, 
            inc : 0       
                   },
          budgetLeft : 0 ,
          percentage : -1
                    };
    
    
    
 return { 
       addItem : function ( type , des , val){
           var ID , newItem;
           if (dataItems.allItems[type].length > 0) {      
               ID = dataItems.allItems[type][dataItems.allItems[type].length-1].id + 1;
                                                    }
           else  {  
               ID = 0;
           }
           if (type === 'exp')
           {
             newItem = new Expense(ID, des , val);
           } else if (type === 'inc')
           {
             newItem = new Income(ID, des , val);
           }
           dataItems.allItems[type].push(newItem);
           
           return newItem;
       },
	 calculateExpPerc: function(){
		 
		 dataItems.allItems.exp.forEach(function(curr){
			 curr.calcPercentage(dataItems.totals.inc);
		 });
		 
	 },
	 getExpPercentage : function(){
		 var perctExp;
		  perctExp = dataItems.allItems.exp.map(function(curr){
			 return curr.percentage;
		 });
		 
		 return perctExp;
	 },
	 
           calculateBudget :function(){
                     //sum of inc and exp individually
                   calculateTotal('exp');
                   calculateTotal('inc');
                     
                    //current budget left after expense deduction
                     dataItems.budgetLeft = Math.abs(dataItems.totals.inc) - Math.abs(dataItems.totals.exp);
                     
                     //calculating percentage of expense made ;
                     if (dataItems.totals.inc > 0){
                     dataItems.percentage = Math.round((dataItems.totals.exp/dataItems.totals.inc)*100);}
                     else {
                               dataItems.percentage = -1;
                     }
           },
	       deleteIncExp : function(type, id ){
			   var ids , idIndex;
			   
			  ids = dataItems.allItems[type].map(function(current){
				   	return current.id;
			   });
//			   return ids;
			   idIndex = ids.indexOf(id);
//			   return idIndex;
			   if( idIndex !== -1 ){
			   dataItems.allItems[type].splice(idIndex,1);
			   };
//			   return dataItems.allItems;
			   
		   },
           
           getBudget : function(){
                   return {  
                     budgetTotal  : dataItems.budgetLeft,
                     budgetPercnt : dataItems.percentage,
                     totalinc     : dataItems.totals.inc,
                     totalexp     : dataItems.totals.exp};          
                               
           },
           test : function(){
                     return dataItems;
           }
           

     
 }   
    
    
})();



//UI Controller
var uiController = (function (){
    var domStrings =  {
        inputType : '.add__value',
        inputDescription :'.add__description',
        expType : '.add__type',
        inputbtn : '.add__btn',
        incomeContainer : '.income__list',
        expenseContainer :'.expenses__list',     
        budgetVal     : '.budget__value',
        budgetIncome  : '.budget__income--value',
        budgetExpPerc : '.budget__expenses--percentage',
        budgetExpense : '.budget__expenses--value',   
        container : '.container',
		item_perc:'.item__percentage',
		budgetmonth:'.budget__title--month'
    };
var formatNum = function(num, type){
	var int, dec , numsplit;
	num = Math.abs(num);
	num = num.toFixed(2);
	
	numsplit = num.split('.');
	int = numsplit[0];
	dec = numsplit[1];
	
	if (int.length>3){
		int = 	int.substr(0,int.length - 3) + ','+int.substr(int.length-3,3)
	}
	return (type === 'exp' ? '- ' : '+ ') + int + '.'+dec;
};
var nodefieldsforeach = function(list, callback){
			   for (var i=0; i< list.length ; i++ )
				   {
					    callback(list[i],i);
				   }
			   
		   };
		   
    
   return {
       getInput : function(){
           return{
        type:          document.querySelector(domStrings.expType).value ,
        description:   document.querySelector(domStrings.inputDescription).value ,
        value:         parseFloat(document.querySelector(domStrings.inputType).value)   };  
               
       },
        adlistItem: function(obj,type){
                 var html , newhtml , element;
                  if (type === 'inc'){
                            element = domStrings.incomeContainer;
                           
                  html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%descr%</div><div class="right clearfix"><div class="item__value">%value%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'}
                  else if (type === 'exp'){
                             element = domStrings.expenseContainer;
                            html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%descr%</div> <div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
                                          }
                  newhtml = html.replace('%id%',obj.id);
                  newhtml = newhtml.replace('%descr%',obj.descr);
                  newhtml = newhtml.replace('%value%',formatNum(obj.value,type));
                  
                 return document.querySelector(element).insertAdjacentHTML('beforeend',newhtml);
                  
                  
        },
        clearFields : function (){
              var fields , fieldsArr ;
                  fields = document.querySelectorAll(domStrings.inputDescription+', '+ domStrings.inputType);
                  fieldsArr = Array.prototype.slice.call(fields);
                  fieldsArr.forEach(function(current,index, array){
                            current.value = '';
                  });
                  fieldsArr[0].focus();
                  
        }, 
	   
	   deleteListItem : function(id){
		   var selectordom = document.getElementById(id)
		   selectordom.parentNode.removeChild(selectordom); 
	   },
	   
	   
	   displayExpPercent: function(percentages){
		   var nodefields = document.querySelectorAll(domStrings.item_perc);
		   
		 
		   nodefieldsforeach(nodefields,function(current,index){
			   if(percentages[index] > 0 ){
				   current.textContent = percentages[index]+'%';
			   }
			   else {
				    current.textContent = '---';
			   }
		   });
		   
	   },
        displayBudget:function(obj){
			var type;
			obj.budgetTotal > 0 ? type = 'inc' : type = 'exp';
			
                  document.querySelector(domStrings.budgetVal).textContent =  formatNum(obj.budgetTotal,type);     
                  document.querySelector(domStrings.budgetIncome).textContent= formatNum(obj.totalinc,type);
                          
                  document.querySelector(domStrings.budgetExpense).textContent= formatNum(obj.totalexp,'exp');
                  if (obj.budgetPercnt > 0){document.querySelector(domStrings.budgetExpPerc).textContent = obj.budgetPercnt;}
                  else{document.querySelector(domStrings.budgetExpPerc).textContent = '---';}
        }, 
	   displaymonth: function(){
		   var newdate , budgetMonth ,budgetYear, budgetMonths ;
		   newdate = new Date();
		   budgetMonth = newdate.getMonth();
		   budgetYear = newdate.getFullYear();
		   budgetMonths = ['January','February','March','April','May','June','July','August','September','October','November','December']
		   document.querySelector(domStrings.budgetmonth).textContent = (budgetMonths[budgetMonth]+' ' +budgetYear)
	   },
	   
	   changeType : function(){
		   var fields = document.querySelectorAll(
		    domStrings.inputType + ','+ domStrings.inputDescription +','+domStrings.expType
		   );
		   nodefieldsforeach(fields,function(curr){
			   curr.classList.toggle('red-focus');
			   
			   
		   });
		   document.querySelector(domStrings.inputbtn).classList.toggle('red-focus');
		   
	   } ,
	   
        getDom : function(){
                    return domStrings ;
        }   
       
       
   } 
    
})(); 



//App  Controller

var appController = (function (budgetContrl,uiContrl){
           var setupListeners = function (){
               var dom = uiContrl.getDom();
               document.querySelector(dom.inputbtn).addEventListener('click',ctrladdEvent);
               document.addEventListener('keypress',function(event){
               if(event.keyCode === 13 || event.which === 13)
                   {
                       ctrladdEvent();
                   }
           });
               document.querySelector(dom.container).addEventListener('click',ctrlDeleteEvent);
               document.querySelector(dom.expType).addEventListener('change',uiContrl.changeType);
                     
           };
          
          var updateBudget = function(){
                 budgetContrl.calculateBudget();
                    
                 var budgetDetails=  budgetContrl.getBudget();
                   // console.log(budgetDetails);
                    
                 uiContrl.displayBudget(budgetDetails);  
                    
          };
          var indExpPercent = function(){
			  budgetContrl.calculateExpPerc();
			  var percExpdetails = budgetContrl.getExpPercentage();
		  		//console.log(percExpdetails);	
			  uiContrl.displayExpPercent(percExpdetails);
		  };
          var ctrladdEvent =  function (){
               //input from ui     
                    var input, newItm, onUI  ;
                input = uiContrl.getInput();
                    
              if (input.description !== "" && !isNaN(input.value) && (input.value > 0))      
              {
                // dataItem stored 
                 newItm = budgetContrl.addItem(input.type, input.description, input.value);
                 onUI = uiContrl.adlistItem(newItm , input.type);
               //fields cleared up
                 uiContrl.clearFields();}
               //budget update on ui 
                  updateBudget(); 
			      indExpPercent();
          
          
          
          }; 
          
          var ctrlDeleteEvent = function(event){
                  var itemDomid,splitID,itemType,itemId;  
               itemDomid  =   event.target.parentNode.parentNode.parentNode.parentNode.id;
               if(itemDomid){
				   splitID = itemDomid.split('-');
				   itemType = splitID[0];
				   itemId = parseInt(splitID[1]);
				   
				  budgetContrl.deleteIncExp(itemType,itemId);
				  
				   //console.log(temp);
				  uiContrl.deleteListItem(itemDomid);
				  updateBudget();
				   indExpPercent();
				   
			   }          
                    
                    
          };
return {
   setinit : function(){
     setupListeners(); 
	   uiContrl.displaymonth(); 	 	
     uiContrl.displayBudget({
                     budgetTotal  : 0,
                     budgetPercnt : 0,
                     totalinc     : 0,
                     totalexp     : 0,
		             percentage: -1
		             
     }) ;       
   }
} 
})(budgetController,uiController);
    
    
appController.setinit();